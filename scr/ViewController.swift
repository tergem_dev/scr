//
//  ViewController.swift
//  SkyrimCharacterRandomizer
//
//  Created by Thomas Müller on 12.07.19.
//  Copyright © 2019 Thomas Müller. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let races : [String] = ["Altmer (High Elf)",
                            "Argonian",
                            "Bosmer (Wood Elf)",
                            "Breton",
                            "Dunmer (Dark Elf)",
                            "Imperial",
                            "Khajit",
                            "Nord",
                            "Orsimer (Orc)",
                            "Redguard"
                            ]
    let gender : [String] = ["Male", "Female"]
    let preset : [String] = ["1","2", "3", "4", "5", "6", "7", "8", "9", "10"]
    @IBOutlet weak var lblRace: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblPreset: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btnRandomizePressed()
    }

    @IBAction func btnRandomizePressed() {
        
        lblRace.text = races[Int.random(in: 0...9)]
        lblGender.text = gender[Int.random(in: 0...1)]
        lblPreset.text = preset[Int.random(in: 0...9)]

    }
    
}

